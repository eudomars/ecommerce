<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'image', 'icon'];

    //Relacion de uno a mucho

    public function subcategories(){
        return $this->hasMany(SubCategory::class);
    }

    //Relacion de mucho a mucho

    public function brands(){
        return $this->belongsToMany(Brands::class);
    }
    

    //Relacion tiene muchos a través de


    public function products(){
        return $this->hasManyThrough(Products::class, SubCategory::class);
    }
}
