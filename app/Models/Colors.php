<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Colors extends Model
{
    use HasFactory;

    //Activar asignacion masiba 
    protected $fillable = ['name'];

    //Relacion mucho a mucho

    public function products(){
        return $this->belongsToMany(Products::class);
    }

    public function sizes(){
        return $this->belongsToMany(Sizes::class);
    }
}
