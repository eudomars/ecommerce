<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sizes extends Model
{
    use HasFactory;

    //Activar asignacion masiba 
    protected $fillable = ['name', 'product_id'];

    //Relacion uno a muuchos inversa

    public function product()
    {
        return $this->belongsTo(Products::class);
    }

    //Relacion mucho a mucho

    public function colors(){
        return $this->belongsToMany(Colors::class);
    }
}
