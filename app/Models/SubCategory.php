<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;

    //Evitamos el almacenamiento de estos campos
    protected $guarded = ['id', 'created_at', 'updated_at'];

    //Relacion de uno a muchos
    public function products(){
        return $this->hasMany(Products::class);
    }

    //Relacion uno a muchos inversoa
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
