<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;


    //Evitaremos la asignacion masiva con la variable guarded
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function sizes(){
        return $this->hasMany(Sizes::class);
    }

    //Relacion uno a mucho inversa
    public function brand(){
        return $this->belongsTo(Brands::class);
    }

    //Relacion de uno a mucho inversa
    public function subcategory(){
        return $this->belongsTo(SubCategory::class);
    }

    //Relacion mucho a mucho
    public function colors(){
        return $this->belongsToMany(Colors::class);
    }

    //Relacion uno a muchos polimorfica
    public function images(){
        return $this->morphMany(image::class, 'imageable');
    }
}
